export const GET_DATA = 'GET_DATA';
export const TOGGLE_STATUS = 'TOGGLE_STATUS';
export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const FILTER_ARRAY = 'FILTER_ARRAY';
