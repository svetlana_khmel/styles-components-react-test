import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../dataActionCreators';
import * as types from '../../constants/actionTypes';
import fetchMock from 'fetch-mock'
import expect from 'expect' // You can use any testing library

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('async actions', () => {
    afterEach(() => {
        fetchMock.reset();
        fetchMock.restore()
    });

    it('creates FETCH_TODOS_SUCCESS when fetching todos has been done', () => {
        fetchMock
            .getOnce('/json', { body: [
                {
                    "id":"0",
                    "status": "undone",
                    "text": "Some plans here"
                },
                {
                    "id":"1",
                    "status": "undone",
                    "text": "Some plans here"
                }
            ]
                , headers: { 'content-type': 'application/json' } });

        const expectedActions = [
            { type: types.GET_DATA, payload: [
                {
                    "id":"0",
                    "status": "undone",
                    "text": "Some plans here"
                },
                {
                    "id":"1",
                    "status": "undone",
                    "text": "Some plans here"
                }
            ]
            }
        ];
        const store = mockStore([]);

        return store.dispatch(actions.loadData()).then(() => {

            // return of async actions
            expect(store.getActions()).toEqual(expectedActions)
        })
    })
});