import * as actions from '../dataActionCreators';
import * as types from '../../constants/actionTypes';

describe('actions', () => {
    it('should create an action get todos', () => {
        const payload = 'Finish docs';
        const expectedAction = {
            type: types.GET_DATA,
            payload
        };
        expect(actions.getData(payload)).toEqual(expectedAction)
    })
});

describe('actions', () => {
    it('should create an action to add a todo', () => {
        const payload = {"id": "5", "status": "undone", "text": "Some plans here1"};
        const expectedAction = {
            type: 'ADD_ITEM',
            payload
        };
        expect(actions.addItem(payload)).toEqual(expectedAction)
    })
});

describe('actions', () => {
    it('should create an action to remove a todo', () => {
        const id = '2';
        const expectedAction = {
            type: types.REMOVE_ITEM,
            id
        };
        expect(actions.removeItem(id)).toEqual(expectedAction)
    })
});

describe('actions', () => {
    it('should create an action to toggle status of todo', () => {
        const id = '1';
        const expectedAction = {
            type: types.TOGGLE_STATUS,
            id
        };
        expect(actions.toggleStatus(id)).toEqual(expectedAction)
    })
});


describe('actions', () => {
    it('should create an action to filter todos', () => {
        const payload = 'Finish docs';
        const expectedAction = {
            type: types.FILTER_ARRAY,
            payload
        };
        expect(actions.filterArray(payload)).toEqual(expectedAction)
    })
});






