import React from 'react';

const Form = ({onSubmit, handleInput}) => {
    return (
        <div>
            <form onSubmit={onSubmit}>
                <input className={'text-input'} type={'text'} onKeyUp={handleInput} />
                <button className={'btn'} type={'submit'}>Add todo </button>
            </form>
        </div>
    )
};

export default Form;