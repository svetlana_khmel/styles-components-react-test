import React, { Component } from 'react';

import { connect } from 'react-redux';
import { filterArray } from '../actions/dataActionCreators';

class Filters extends Component {

    filterData (status) {
      //  let filter = {'status': status};
        debugger;
        this.props.filterArray(filter);
    }

    render() {
        const {status} = this.props.filter;

        return (
            <div className='filters'>
                <div onClick={() => this.filterData('all')} className={status === 'all' ? 'active filter all' : 'filter all'}>All</div>
                <div onClick={() => this.filterData('undone')} className={status === 'undone' ? 'active filter undone' : 'filter undone'}>Not completed</div>
                <div onClick={() => this.filterData('done')} className={status === 'done' ? 'active filter done' : 'filter done'}>Completed</div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {

    return {
        data: state.data,
        filter: state.filter
    }
};

const mapDispatchToProps = (dispatch) => {
    return ({
        filterArray: (status) => {dispatch(filterArray(status))}
    })
};

export default connect(mapStateToProps, mapDispatchToProps)(Filters);


