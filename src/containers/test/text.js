import React, {Component} from 'react';

class TestTextComponent extends Component {
  constructor (props) {
    super(props);

  }

  render() {
    console.log('THIS PROPS', this.props)
    return (
        <div>{this.props.text}</div>

    );
  }
}
export default TestTextComponent;
