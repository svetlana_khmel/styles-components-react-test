// import TestBannerComponent from './banner';
// import TestTextComponent from './text';
//
//
// export { TestBannerComponent, TestTextComponent } ;

import TestBannerComponent from './banner';
import TestTextComponent from './text';

module.exports = {
  TestBannerComponent: TestBannerComponent,
  TestTextComponent: TestTextComponent
}