import React, {Component} from 'react';
//import reset from 'reset-css';
import './css/reset.scss';
import styled from 'styled-components';

// Create a <Wrapper> react component that renders a <section> with
// some padding and a papayawhip background
const Wrapper = styled.section`
  padding: 4em;
  background: papayawhip;
`;


class TestBannerComponent extends Component {
  constructor () {
    super();

    this.state = {
      width: 0,
      height: 0,
      url:'',
      text: 'test',
      html: 'html',
      scripts: 'scripts'
    }
  }

  componentDidMount() {
    const api = `http://back-for-test-banner-react-npm.herokuapp.com/api`;

    fetch(api)
      .then(response => response.json())
      .then(data => {
        const { width, height, url } = data.banners[0];
        let rawhtml = data.banners[0].html;
        let rawscripts = data.banners[0].scripts;


        let html = rawhtml.replace(/\\\"/g, '"');

        console.log(html);

        window.WEBPACK = (function () {
          let scripts = rawscripts ? rawscripts.replace(/\\\"/g, '"') : '';
          let evalScripts = eval(scripts);

          console.log('here');

          window.onload = (function () {
            typeof evalScripts === 'function' ?  evalScripts() : '';
            console.log('script has been loaded..! ')
          })(this);

          // let head = document.getElementsByTagName('head')[0];
          // let script = document.createElement('script');
          // script.type= 'text/javascript';
          // script.src= 'helper.js';
          // head.appendChild(script);
          //
          // script.onload = (function () {
          //   console.log('script has been loaded..! ')
          // })(this);


        })(this);

        this.setState({width, height, url, html});
      });
  }

  render() {

    if (this.state.width === 0) {
      return <div>Loading...</div>
    }

    return (
      <Wrapper>
        <div class={'test-css'} style={{ width:`${this.state.width}px`, height:`${this.state.height}px`, backgroundImage: `url(${this.state.url})`, backgroundSize: 'contain'}}>
          <div dangerouslySetInnerHTML={{__html: this.state.html}}></div>
          <div>{this.state.text}</div>
        </div>
      </Wrapper>
    );
  }
}
export default TestBannerComponent;
