import React from 'react'
import { mount } from 'enzyme'
import App from '../App';

import Adapter from 'enzyme-adapter-react-15';

configure({ adapter: new Adapter() });

function setup() {
    const props = {
        //addTodo: jest.fn()
        addItem: jest.fn()
    };

    const enzymeWrapper = mount(<App {...props} />)

    return {
        props,
        enzymeWrapper
    }
}

describe('components', () => {
    describe('List', () => {
        it('should render self and subcomponents', () => {
            const { enzymeWrapper } = setup();

            expect(enzymeWrapper.find('div').hasClass('todo-lis')).toBe(true);
            expect(enzymeWrapper.find('Form')).toBe(true);
            expect(enzymeWrapper.find('List')).toBe(true);
            expect(enzymeWrapper.find('Filters')).toBe(true);

            expect(enzymeWrapper.find('h1').text()).toBe('Todo list');

            const todoInputProps = enzymeWrapper.find('Form').props();
            expect(todoInputProps.onSubmit).toBe(this.onSubmit);
           // expect(todoInputProps.placeholder).toEqual('What needs to be done?')
        })

        // it('should call addTodo if length of text is greater than 0', () => {
        //     const { enzymeWrapper, props } = setup();
        //     const input = enzymeWrapper.find('TodoTextInput')
        //     input.props().onSave('')
        //     expect(props.addTodo.mock.calls.length).toBe(0)
        //     input.props().onSave('Use Redux')
        //     expect(props.addTodo.mock.calls.length).toBe(1)
        // })
    })
})